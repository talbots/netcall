var net = require("net");

var protocol = {};

var Split = function(data){
  var found = false;
  var i = 0;
  var method = "";
  while(!found && i < data.length){
    if(data[i] == '\n'){
      found = true;
    }
    else{
      method += data[i];
    }
    i++;
  }
  if(!found){
    throw "Requête invalide, la requête doit contenir un retour ligne";
  }
  var content = "";
  for(j = i; j < data.length; j++){
    content += data[j];
  }
  var backData = {
    "method": method,
    "content" : content
  }
  return backData;
}
var Check = function(method){
  var i = 0;
  var valid = true;
  while(valid && i < method.length){
    valid = !(method[i] == '\n');
    i++;
  }
  return valid;
}
var Merge = function(method, data){
  var merged = method + '\n' + data;
  return merged;
}

protocol.Server = function(fn){
  this.fn = fn;
  this.listen = function(port){
    var func = this.fn;
    net.createServer(function(socket){
      socket.on('data', function(data){
        try{
          var split = Split(data.toString('utf8'));
          var backdata = func(split.method, split.content);
          socket.write(backdata);
        }
        catch(e){
          socket.write("BAD REQUEST");
        }
        socket.pipe(socket);
      })
    }).listen(port);
  }
}

protocol.Client = function(port, ip){
  this.port = port;
  this.ip = ip;
  this.Call = function(method, data, fn){
    if(!Check(method)){
      throw "La méthode ne peut contenir un retour ligne";
    }
    var tcpclient = new net.Socket();
    tcpclient.connect(this.port, this.ip, function(){
      tcpclient.write(Merge(method, data));
    });
    tcpclient.on('data', function(data){
      fn(data.toString('utf8'));
      tcpclient.destroy();
    });
  }
}

module.exports = protocol;
